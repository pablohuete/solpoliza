﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliza.Entidades
{
    public class Cobertura
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public double Tarifa { get; set; }

        public Cobertura(string codigo, string descripcion, double tarifa)
        {
            this.Codigo = codigo;
            this.Descripcion = descripcion;
            this.Tarifa = tarifa;
        }
        public Cobertura()
        {

        }
    }
}
