﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliza.Entidades
{
    public class Adiccion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

        public Adiccion(string descripcion)
        {
            this.Descripcion = descripcion;
        }
    }
}
