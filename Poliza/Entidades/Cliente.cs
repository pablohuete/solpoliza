﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliza.Entidades
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Genero { get; set; }
        public double MontoAsegurado { get; set; }
        public List<Adiccion> Adicciones { get; set; }
        public List<Cobertura> Coberturas { get; set; }

        public Cliente(string cedula, string nombre, DateTime fechaNacimiento, string genero, double montoAsegurado)
        {
            this.Cedula = cedula;
            this.Nombre = nombre;
            this.FechaNacimiento = fechaNacimiento;
            this.Genero = genero;
            this.MontoAsegurado = montoAsegurado;
        }
        public Cliente()
        {

        }

        public List<ClienteCobertura> ObtenerCoberturas()
        {
            throw new NotSupportedException();
        }
    }
}
