﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliza.Entidades
{
    public class ClienteCobertura : Cobertura
    {
        public double MontoAsegurado { get; set; }

        public ClienteCobertura(string codigo, string descripcion, double tarifa, double montoAsegurado)
          : base(codigo, descripcion, tarifa)
        {
            this.MontoAsegurado = montoAsegurado;
        }

        public double ObtenerMontoTotal()
        {
            throw new  NotSupportedException();
        }
    }
}
