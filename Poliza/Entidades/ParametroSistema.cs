﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poliza.Entidades
{
    public class ParametroSistema
    {
        public int Id { get; set; }
        public int EdadMinima { get; set; }
        public int EdadMedia { get; set; }
        public int EdadMaxima { get; set; }
        public double PorcentajeRecargoMasculino { get; set; }
        public double PorcentajeRecargoEdadMedia { get; set; }
        public int CantidadMinimaAdicciones { get; set; }
        public double PorcentajeMinimoAdicciones { get; set; }
        public int CantidadMediaAdicciones { get; set; }
        public double PorcentajeMedioAdicciones { get; set; }
        public int CantidadMaximaAdicciones { get; set; }
        public double PorcentajeMaximoAdicciones { get; set; }
        public double MontoMinimoSeguro { get; set; }
        public double MontoMaximoSeguro { get; set; }

    }
}
