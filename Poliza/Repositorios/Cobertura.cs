﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliza.Repositorios.Datos;
using System.Data;
using NpgsqlTypes;

namespace Poliza.Repositorios
{
    public static class Cobertura
    {
        public static List<Entidades.Cobertura> ObtenerCoberturas(int idCliente = 0)
        {
            List<Entidades.Cobertura> coberturas = new List<Entidades.Cobertura>();
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            DataSet dataSet = new DataSet();
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".cobertura";
                if (idCliente != 0)
                {
                    Parametro oParametro = new Parametro();
                    sql = "SELECT * FROM " + cnx.Schema + ".cobertura" +
                        "ON id = (SELECT id_cobertura FROM " + cnx.Schema + ".cliente_cobertura" +
                        " WHERE id_cliente = @idCliente);";

                    oParametro.agregarParametro("@idCliente", NpgsqlDbType.Integer, idCliente);
                    dataSet = cnx.ejecutarConsultaSQL(sql, "cobertura", oParametro.obtenerParametros());
                }
                else
                {
                    dataSet = cnx.ejecutarConsultaSQL(sql);
                }
                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        coberturas.Add(new Entidades.Cobertura
                        {
                            Id = Int32.Parse(fila["id"].ToString()),
                            Codigo = fila["codigo"].ToString(),
                            Descripcion = fila["descripcion"].ToString(),
                            Tarifa = Double.Parse(fila["tarifa"].ToString())
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return coberturas;
        }
    }
}
