﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Poliza.Repositorios.Datos;


namespace Poliza.Repositorios
{
    public static class ParametroSistema
    {
        public static Entidades.ParametroSistema ObtenerParametros()
        {
            Entidades.ParametroSistema parametroSistema = new Entidades.ParametroSistema();
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            try
            {
                string sql = "SELECT * FROM " + cnx.Schema + ".parametro_sistema WHERE " +
                    " id IN(SELECT MAX(id) AS ultima_fila FROM " + cnx.Schema + ".parametro_sistema);";
                DataSet dataSet;
                dataSet = cnx.ejecutarConsultaSQL(sql);

                if (dataSet.Tables.Count > 0)
                {
                    DataRow fila = dataSet.Tables[0].Rows[0];
                    parametroSistema.Id = Int32.Parse(fila["id"].ToString());
                    parametroSistema.EdadMinima = Int32.Parse(fila["edad_minima"].ToString());
                    parametroSistema.EdadMedia = Int32.Parse(fila["edad_media"].ToString());
                    parametroSistema.EdadMaxima = Int32.Parse(fila["edad_maxima"].ToString());
                    parametroSistema.PorcentajeRecargoMasculino = Double.Parse(fila["porcentaje_recargo_masculino"].ToString());
                    parametroSistema.PorcentajeRecargoEdadMedia = Double.Parse(fila["porcentaje_recargo_edad_media"].ToString());
                    parametroSistema.CantidadMinimaAdicciones = Int32.Parse(fila["cantidad_minima_adicciones"].ToString());
                    parametroSistema.PorcentajeMinimoAdicciones = Double.Parse(fila["porcentaje_minimo_adicciones"].ToString());
                    parametroSistema.CantidadMediaAdicciones = Int32.Parse(fila["cantidad_media_adicciones"].ToString());
                    parametroSistema.PorcentajeMedioAdicciones = Double.Parse(fila["porcentaje_medio_adicciones"].ToString());
                    parametroSistema.CantidadMaximaAdicciones = Int32.Parse(fila["cantidad_maxima_adicciones"].ToString());
                    parametroSistema.PorcentajeMaximoAdicciones = Double.Parse(fila["porcentaje_maximo_adicciones"].ToString());
                    parametroSistema.MontoMinimoSeguro = Double.Parse(fila["monto_minimo_seguro"].ToString());
                    parametroSistema.MontoMaximoSeguro = Double.Parse(fila["monto_maximo_seguro"].ToString());
               
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return parametroSistema;
        }
    }
}
