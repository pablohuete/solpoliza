﻿using System;
using System.Collections.Generic;
using Poliza.Repositorios.Datos;
using System.Data;
using NpgsqlTypes;
namespace Poliza.Repositorios
{
    public class Cliente
    {
        public List<Entidades.Cliente> ObtenerClientes(string filtro = "")
        {
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            List<Entidades.Cliente> clientes = new List<Entidades.Cliente>();
            try
            {
                DataSet dataSet = new DataSet();
                string sql = "SELECT * FROM " + cnx.Schema + ".cliente;";
                Parametro oParametro = new Parametro();
                if (!String.IsNullOrEmpty(filtro))
                {
                   
                    sql = "SELECT * FROM " + cnx.Schema + ".cliente" +
                        " WHERE nombre = @nombre OR cedula = @cedula;";
                    oParametro.agregarParametro("@nombre", NpgsqlDbType.Varchar, filtro);
                    oParametro.agregarParametro("@cedula", NpgsqlDbType.Varchar, filtro);
                }
                dataSet = cnx.ejecutarConsultaSQL(sql,"cliente",oParametro.obtenerParametros());
                if (dataSet.Tables.Count > 0)
                {
                    Entidades.Cliente oCliente = new Entidades.Cliente();
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        oCliente.Id = Int32.Parse(fila["id"].ToString());
                        oCliente.Cedula = fila["cedula"].ToString();
                        oCliente.Nombre = fila["nombre"].ToString();
                        oCliente.FechaNacimiento = (DateTime)fila["fecha_nacimiento"];
                        oCliente.Genero = fila["genero"].ToString();
                        oCliente.MontoAsegurado = Double.Parse(fila["monto_asegurado"].ToString());
                        oCliente.Coberturas = Cobertura.ObtenerCoberturas(oCliente.Id);

                        clientes.Add(oCliente);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clientes;
        }
    }
}
