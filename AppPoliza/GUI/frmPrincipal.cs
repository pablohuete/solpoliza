﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Poliza.Entidades;
using appPoliza.BOL;

namespace appPoliza.GUI
{
    public partial class frmPrincipal : Form
    {
        private List<Cliente> clientes;
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            try
            {
                BolCliente bolCliente = new BolCliente();
                this.clientes = bolCliente.ObtenerClientes();
                this.dgvClientes.DataSource = this.clientes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tsbtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmResgistrarPoliza frmResgistrar = new frmResgistrarPoliza();
                frmResgistrar.ShowDialog();
                if (frmResgistrar.DialogResult == DialogResult.OK)
                {
                    MessageBox.Show("Agregado Correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Intente de nuevo", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this.Show();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
