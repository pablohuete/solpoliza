﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Poliza.Entidades;
using appPoliza.BOL;

namespace appPoliza.GUI
{
    public partial class frmResgistrarPoliza : Form
    {
        private ParametroSistema parametroSistema;
        private int edadCliente = 0;
        public Cliente oCliente { get; set; }
        public frmResgistrarPoliza()
        {
            InitializeComponent();
        }

        private void frmResgistrarPoliza_Load(object sender, EventArgs e)
        {
            try
            {
                BolParametroSistema bolParametro = new BolParametroSistema();
                this.parametroSistema = bolParametro.ObtenerParametros();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }          
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtpFechaNacimiento_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime fechaObtenida = this.dtpFechaNacimiento.Value;
                this.edadCliente = DateTime.Now.Year - fechaObtenida.Year;
                this.txtEdad.Text = this.edadCliente.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }

        private void dtpFechaNacimiento_CloseUp(object sender, EventArgs e)
        {
            
            if (this.edadCliente < parametroSistema.EdadMinima 
                || this.edadCliente > parametroSistema.EdadMaxima)
            {
                MessageBox.Show("Edad no Admitida!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {

        }
    }
}
