﻿namespace appPoliza.GUI
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbtnAgregar = new System.Windows.Forms.ToolStripButton();
            this.tsbtnModificar = new System.Windows.Forms.ToolStripButton();
            this.tsbtnEliminar = new System.Windows.Forms.ToolStripButton();
            this.tsbtnRefrescar = new System.Windows.Forms.ToolStripButton();
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.Columna_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columna_Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columna_FechaNacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columna_Genero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columna_Monto_Asegurado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnAgregar,
            this.tsbtnModificar,
            this.tsbtnEliminar,
            this.tsbtnRefrescar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(783, 38);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbtnAgregar
            // 
            this.tsbtnAgregar.Image = global::appPoliza.Properties.Resources.persons;
            this.tsbtnAgregar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAgregar.Name = "tsbtnAgregar";
            this.tsbtnAgregar.Size = new System.Drawing.Size(93, 35);
            this.tsbtnAgregar.Text = "Agregar Cliente";
            this.tsbtnAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbtnAgregar.Click += new System.EventHandler(this.tsbtnAgregar_Click);
            // 
            // tsbtnModificar
            // 
            this.tsbtnModificar.Image = global::appPoliza.Properties.Resources.Modify;
            this.tsbtnModificar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnModificar.Name = "tsbtnModificar";
            this.tsbtnModificar.Size = new System.Drawing.Size(95, 35);
            this.tsbtnModificar.Text = "Modificar Datos";
            this.tsbtnModificar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbtnEliminar
            // 
            this.tsbtnEliminar.Image = global::appPoliza.Properties.Resources.Delete;
            this.tsbtnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnEliminar.Name = "tsbtnEliminar";
            this.tsbtnEliminar.Size = new System.Drawing.Size(54, 35);
            this.tsbtnEliminar.Text = "Eliminar";
            this.tsbtnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbtnRefrescar
            // 
            this.tsbtnRefrescar.Image = global::appPoliza.Properties.Resources.Refresh;
            this.tsbtnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnRefrescar.Name = "tsbtnRefrescar";
            this.tsbtnRefrescar.Size = new System.Drawing.Size(59, 35);
            this.tsbtnRefrescar.Text = "Refrescar";
            this.tsbtnRefrescar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dgvClientes
            // 
            this.dgvClientes.AllowUserToAddRows = false;
            this.dgvClientes.AllowUserToDeleteRows = false;
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Columna_Id,
            this.Columna_Cedula,
            this.Columna_FechaNacimiento,
            this.Columna_Genero,
            this.Columna_Monto_Asegurado});
            this.dgvClientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvClientes.Location = new System.Drawing.Point(0, 38);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.ReadOnly = true;
            this.dgvClientes.Size = new System.Drawing.Size(783, 422);
            this.dgvClientes.TabIndex = 1;
            // 
            // Columna_Id
            // 
            this.Columna_Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Id.HeaderText = "Id";
            this.Columna_Id.Name = "Columna_Id";
            this.Columna_Id.ReadOnly = true;
            this.Columna_Id.Width = 41;
            // 
            // Columna_Cedula
            // 
            this.Columna_Cedula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Cedula.HeaderText = "Cédula";
            this.Columna_Cedula.Name = "Columna_Cedula";
            this.Columna_Cedula.ReadOnly = true;
            this.Columna_Cedula.Width = 65;
            // 
            // Columna_FechaNacimiento
            // 
            this.Columna_FechaNacimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_FechaNacimiento.HeaderText = "Fecha de Nacimiento";
            this.Columna_FechaNacimiento.Name = "Columna_FechaNacimiento";
            this.Columna_FechaNacimiento.ReadOnly = true;
            this.Columna_FechaNacimiento.Width = 122;
            // 
            // Columna_Genero
            // 
            this.Columna_Genero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Genero.HeaderText = "Género";
            this.Columna_Genero.Name = "Columna_Genero";
            this.Columna_Genero.ReadOnly = true;
            this.Columna_Genero.Width = 67;
            // 
            // Columna_Monto_Asegurado
            // 
            this.Columna_Monto_Asegurado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Monto_Asegurado.HeaderText = "Monto Asegurado";
            this.Columna_Monto_Asegurado.Name = "Columna_Monto_Asegurado";
            this.Columna_Monto_Asegurado.ReadOnly = true;
            this.Columna_Monto_Asegurado.Width = 106;
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 460);
            this.Controls.Add(this.dgvClientes);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listado de Clientes";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbtnAgregar;
        private System.Windows.Forms.ToolStripButton tsbtnModificar;
        private System.Windows.Forms.ToolStripButton tsbtnEliminar;
        private System.Windows.Forms.ToolStripButton tsbtnRefrescar;
        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_FechaNacimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Genero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Monto_Asegurado;
    }
}