﻿namespace appPoliza.GUI
{
    partial class frmResgistrarPoliza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvDetalles = new System.Windows.Forms.DataGridView();
            this.Columna_Cobertura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Columna_Prima = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.txtPrimaAPagar = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMontoAsegurado = new System.Windows.Forms.TextBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbAdicciones = new System.Windows.Forms.GroupBox();
            this.lvAdicciones = new System.Windows.Forms.ListView();
            this.gbCoberturas = new System.Windows.Forms.GroupBox();
            this.dgvCoberturas = new System.Windows.Forms.DataGridView();
            this.lblMontoAsegurado = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.lblEdad = new System.Windows.Forms.Label();
            this.cbGenero = new System.Windows.Forms.ComboBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.lblGenero = new System.Windows.Forms.Label();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.lblCedula = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).BeginInit();
            this.panel2.SuspendLayout();
            this.gbAdicciones.SuspendLayout();
            this.gbCoberturas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoberturas)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 516);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Registro de Póliza";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvDetalles);
            this.panel1.Controls.Add(this.btnGuardar);
            this.panel1.Controls.Add(this.txtPrimaAPagar);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtMontoAsegurado);
            this.panel1.Controls.Add(this.btnCalcular);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.lblMontoAsegurado);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 510);
            this.panel1.TabIndex = 2;
            // 
            // dgvDetalles
            // 
            this.dgvDetalles.AllowUserToAddRows = false;
            this.dgvDetalles.AllowUserToDeleteRows = false;
            this.dgvDetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Columna_Cobertura,
            this.Columna_Prima});
            this.dgvDetalles.Location = new System.Drawing.Point(455, 346);
            this.dgvDetalles.Name = "dgvDetalles";
            this.dgvDetalles.ReadOnly = true;
            this.dgvDetalles.Size = new System.Drawing.Size(297, 120);
            this.dgvDetalles.TabIndex = 10;
            // 
            // Columna_Cobertura
            // 
            this.Columna_Cobertura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Cobertura.HeaderText = "Cobertura";
            this.Columna_Cobertura.Name = "Columna_Cobertura";
            this.Columna_Cobertura.ReadOnly = true;
            this.Columna_Cobertura.Width = 87;
            // 
            // Columna_Prima
            // 
            this.Columna_Prima.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Columna_Prima.HeaderText = "Prima";
            this.Columna_Prima.Name = "Columna_Prima";
            this.Columna_Prima.ReadOnly = true;
            this.Columna_Prima.Width = 63;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(316, 465);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtPrimaAPagar
            // 
            this.txtPrimaAPagar.Location = new System.Drawing.Point(535, 472);
            this.txtPrimaAPagar.Name = "txtPrimaAPagar";
            this.txtPrimaAPagar.Size = new System.Drawing.Size(217, 20);
            this.txtPrimaAPagar.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(453, 475);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Prima a Pagar:";
            // 
            // txtMontoAsegurado
            // 
            this.txtMontoAsegurado.Location = new System.Drawing.Point(125, 365);
            this.txtMontoAsegurado.Name = "txtMontoAsegurado";
            this.txtMontoAsegurado.Size = new System.Drawing.Size(164, 20);
            this.txtMontoAsegurado.TabIndex = 5;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCalcular.Image = global::appPoliza.Properties.Resources.if_hand_o_right_16089201;
            this.btnCalcular.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCalcular.Location = new System.Drawing.Point(329, 365);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(62, 52);
            this.btnCalcular.TabIndex = 4;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCalcular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbAdicciones);
            this.panel2.Controls.Add(this.gbCoberturas);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(785, 210);
            this.panel2.TabIndex = 3;
            // 
            // gbAdicciones
            // 
            this.gbAdicciones.Controls.Add(this.lvAdicciones);
            this.gbAdicciones.ForeColor = System.Drawing.SystemColors.Highlight;
            this.gbAdicciones.Location = new System.Drawing.Point(394, 6);
            this.gbAdicciones.Name = "gbAdicciones";
            this.gbAdicciones.Size = new System.Drawing.Size(361, 201);
            this.gbAdicciones.TabIndex = 2;
            this.gbAdicciones.TabStop = false;
            this.gbAdicciones.Text = "Adicciones";
            // 
            // lvAdicciones
            // 
            this.lvAdicciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAdicciones.Location = new System.Drawing.Point(3, 16);
            this.lvAdicciones.Name = "lvAdicciones";
            this.lvAdicciones.Size = new System.Drawing.Size(355, 182);
            this.lvAdicciones.TabIndex = 6;
            this.lvAdicciones.UseCompatibleStateImageBehavior = false;
            this.lvAdicciones.View = System.Windows.Forms.View.List;
            // 
            // gbCoberturas
            // 
            this.gbCoberturas.Controls.Add(this.dgvCoberturas);
            this.gbCoberturas.ForeColor = System.Drawing.SystemColors.Highlight;
            this.gbCoberturas.Location = new System.Drawing.Point(20, 6);
            this.gbCoberturas.Name = "gbCoberturas";
            this.gbCoberturas.Size = new System.Drawing.Size(343, 201);
            this.gbCoberturas.TabIndex = 1;
            this.gbCoberturas.TabStop = false;
            this.gbCoberturas.Text = "Coberturas";
            // 
            // dgvCoberturas
            // 
            this.dgvCoberturas.AllowUserToAddRows = false;
            this.dgvCoberturas.AllowUserToDeleteRows = false;
            this.dgvCoberturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCoberturas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCoberturas.Location = new System.Drawing.Point(3, 16);
            this.dgvCoberturas.Name = "dgvCoberturas";
            this.dgvCoberturas.ReadOnly = true;
            this.dgvCoberturas.Size = new System.Drawing.Size(337, 182);
            this.dgvCoberturas.TabIndex = 0;
            // 
            // lblMontoAsegurado
            // 
            this.lblMontoAsegurado.AutoSize = true;
            this.lblMontoAsegurado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoAsegurado.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblMontoAsegurado.Location = new System.Drawing.Point(29, 368);
            this.lblMontoAsegurado.Name = "lblMontoAsegurado";
            this.lblMontoAsegurado.Size = new System.Drawing.Size(94, 13);
            this.lblMontoAsegurado.TabIndex = 2;
            this.lblMontoAsegurado.Text = "Monto Asegurado:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtEdad);
            this.groupBox1.Controls.Add(this.lblEdad);
            this.groupBox1.Controls.Add(this.cbGenero);
            this.groupBox1.Controls.Add(this.dtpFechaNacimiento);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.txtCedula);
            this.groupBox1.Controls.Add(this.lblGenero);
            this.groupBox1.Controls.Add(this.lblFechaNacimiento);
            this.groupBox1.Controls.Add(this.lblCedula);
            this.groupBox1.Controls.Add(this.lblNombre);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(785, 130);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cliente";
            // 
            // txtEdad
            // 
            this.txtEdad.Enabled = false;
            this.txtEdad.Location = new System.Drawing.Point(32, 104);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Size = new System.Drawing.Size(100, 20);
            this.txtEdad.TabIndex = 8;
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblEdad.Location = new System.Drawing.Point(29, 79);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 7;
            this.lblEdad.Text = "Edad";
            // 
            // cbGenero
            // 
            this.cbGenero.FormattingEnabled = true;
            this.cbGenero.Items.AddRange(new object[] {
            "F",
            "M"});
            this.cbGenero.Location = new System.Drawing.Point(551, 43);
            this.cbGenero.Name = "cbGenero";
            this.cbGenero.Size = new System.Drawing.Size(92, 21);
            this.cbGenero.TabIndex = 1;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.CustomFormat = "dd/mm/yy";
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(360, 44);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(105, 20);
            this.dtpFechaNacimiento.TabIndex = 6;
            this.dtpFechaNacimiento.CloseUp += new System.EventHandler(this.dtpFechaNacimiento_CloseUp);
            this.dtpFechaNacimiento.ValueChanged += new System.EventHandler(this.dtpFechaNacimiento_ValueChanged);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(189, 47);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 5;
            // 
            // txtCedula
            // 
            this.txtCedula.Location = new System.Drawing.Point(32, 44);
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(100, 20);
            this.txtCedula.TabIndex = 4;
            // 
            // lblGenero
            // 
            this.lblGenero.AutoSize = true;
            this.lblGenero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenero.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblGenero.Location = new System.Drawing.Point(548, 16);
            this.lblGenero.Name = "lblGenero";
            this.lblGenero.Size = new System.Drawing.Size(42, 13);
            this.lblGenero.TabIndex = 3;
            this.lblGenero.Text = "Género";
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaNacimiento.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(357, 16);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(108, 13);
            this.lblFechaNacimiento.TabIndex = 2;
            this.lblFechaNacimiento.Text = "Fecha de Nacimiento";
            // 
            // lblCedula
            // 
            this.lblCedula.AutoSize = true;
            this.lblCedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCedula.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCedula.Location = new System.Drawing.Point(29, 16);
            this.lblCedula.Name = "lblCedula";
            this.lblCedula.Size = new System.Drawing.Size(40, 13);
            this.lblCedula.TabIndex = 1;
            this.lblCedula.Text = "Cédula";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblNombre.Location = new System.Drawing.Point(186, 16);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(799, 542);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Tag = "";
            // 
            // frmResgistrarPoliza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 542);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "frmResgistrarPoliza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro de Póliza de Vida";
            this.Load += new System.EventHandler(this.frmResgistrarPoliza_Load);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetalles)).EndInit();
            this.panel2.ResumeLayout(false);
            this.gbAdicciones.ResumeLayout(false);
            this.gbCoberturas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoberturas)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtPrimaAPagar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMontoAsegurado;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbAdicciones;
        private System.Windows.Forms.ListView lvAdicciones;
        private System.Windows.Forms.GroupBox gbCoberturas;
        private System.Windows.Forms.DataGridView dgvCoberturas;
        private System.Windows.Forms.Label lblMontoAsegurado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbGenero;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCedula;
        private System.Windows.Forms.Label lblGenero;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.Label lblCedula;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.DataGridView dgvDetalles;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Cobertura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Columna_Prima;
    }
}

