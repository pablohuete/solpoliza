﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliza.Entidades;

namespace appPoliza.BOL
{
    public class BolParametroSistema
    {
        public ParametroSistema ObtenerParametros()
        {
            ParametroSistema parametroSistema;
            try
            {
                parametroSistema = Poliza.Repositorios.ParametroSistema.ObtenerParametros();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return parametroSistema;
        }
    }
}
