﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poliza.Entidades;

namespace appPoliza.BOL
{
    class BolCliente
    {
        //public int CrearCliente()
        //{

        //}

        public List<Cliente> ObtenerClientes()
        {
            List<Cliente> clientes = new List<Cliente>();
            try
            {
                Poliza.Repositorios.Cliente dalCliente = new Poliza.Repositorios.Cliente();
                clientes = dalCliente.ObtenerClientes();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clientes;
        }
    }
}
